<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2 PHP</title>
    <?php
        function comprobarVocales($palabra){
            $existeA = false;
            $existeE = false;
            $existeI = false;
            $existeO = false;
            $existeU = false;

            for($i=0;$i<strlen($palabra);$i++){
                $letra = $palabra[$i];
                if($letra == 'a') $existeA = true;
                if($letra == 'A') $existeA = true;
                if($letra == 'e') $existeE = true;
                if($letra == 'E') $existeE = true;
                if($letra == 'i') $existeI = true;
                if($letra == 'I') $existeI = true;
                if($letra == 'o') $existeO = true;
                if($letra == 'O') $existeO = true;
                if($letra == 'u') $existeU = true;
                if($letra == 'U') $existeU = true;
            }
            
            if($existeA && $existeE && $existeI && $existeO && $existeU) return true;
            else return false;
        }
    ?>
</head>
<body>

    <form method="POST" action="index.php">
        <label>Escribe algo: </label>
        <input type="text" name='palabra'/>
        <input type="submit" />
    </form>

    <?php
        if(isset($_POST['palabra'])){
            if(comprobarVocales($_POST['palabra'])) echo "<p style='color:green'>LA PALABRA CONTIENE LAS 5 VOCALES</p>";
            else echo "<p style='color:red'>NO CONTIENE TODAS LAS VOCALES</p>";
        }
    ?>

</body>
</html>